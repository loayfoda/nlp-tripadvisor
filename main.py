# -*- coding: utf-8 -*-
"""
Created on Tue Feb 11 18:05:18 2020

@author: LF05594
"""

import logging
import logging.config
from WebScrapping import ReadData





def main():
    logging.config.fileConfig('conf/logging.conf',disable_existing_loggers=False)
    URL = 'https://www.tripadvisor.com/Restaurant_Review-g187291-d1447547-Reviews-Cortijo_Restaurant_Tapasbar-Stuttgart_Baden_Wurttemberg.html'
    ReadData.get_reviews(URL)



if __name__=='__main__':
    main()