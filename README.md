# Tripadvisor Reviews Scrapper

The main idea behind this project is to perform Entity & sentiment analysis on TripAdvisor reviews to identify and categorize opinions & reviews expressed regarding a specific restaurant.

**Technical specs**

**1.** The project uses the "Google NLP API", as well as "NLTK Vader" for the Entity & Sentiment analysis

**2.** The project uses "Beautifulsoup" & "Selenium" to scrape the reviews from Tripadvisor


![arch](/uploads/ee9cc899366df32f59bd4aa569afc738/arch.png)
 



### Prerequisites

* Python Environment
* Google Cloud Project with enabled NLP API
* Different libs used throughout the project


### Installing

A step by step series of examples that tell you how to get a development env running

1- Download the Git Repository to a known directory

```
git clone .....
```

2- Open the project root folder using any python editor 

3- Add your own Google credentials file in the "conf" folder

4- Run the "main.py" to scrape tripadvisor reviews

```
Reviews are saved in a CSV file called output
```
![reviews](/uploads/155272c48e7a5e5db7d8550d4a450a3c/reviews.PNG)

5- Open "SentimentAnalysis.ipynb"(a jupyter notebook file) and start executing the cells to apply the Entity and sentiment analysis, as well as producing couple of graphs to visualize the analysis

![pred](/uploads/dd9120cadf8ea99c0cabde0329d64132/pred.PNG)

![cloudword](/uploads/eb81ac6d353d96d5948f24d23d017fb3/cloudword.PNG)![occ](/uploads/c287246f3652b8d19df9bd29ef5c928c/occ.PNG)

## Built With

* [Spyder](https://www.spyder-ide.org/) - Python Development Environment


## Authors

* **Loay Foda** - *Initial work* - [Loay Foda](https://gitlab.com/loayfoda)


## License

This project is licensed under the MIT License - see the [LICENSE.txt](LICENSE.txt) file for details



