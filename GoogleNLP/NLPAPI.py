# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 11:29:19 2020

@author: LF05594
"""
import config
import os
# Imports the Google Cloud client library

from google.cloud.language import enums
from google.cloud import language_v1
import pandas as pd
import logging
log = logging.getLogger(__name__)

#Authenticate with google cloud
os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config.google_credentials
log.info(os.environ['GOOGLE_APPLICATION_CREDENTIALS'])

def sample_analyze_entity_sentiment(text_content):
    """
    Analyzing Entity Sentiment in a String

    Args:
      text_content The text content to analyze
    """

    client = language_v1.LanguageServiceClient()

    # text_content = 'Grapes are good. Bananas are bad.'

    # Available types: PLAIN_TEXT, HTML
    type_ = enums.Document.Type.PLAIN_TEXT

    # Optional. If not specified, the language is automatically detected.
    # For list of supported languages:
    # https://cloud.google.com/natural-language/docs/languages
    language = "en"
    document = {"content": text_content, "type": type_, "language": language}

    # Available values: NONE, UTF8, UTF16, UTF32
    encoding_type = enums.EncodingType.UTF8
    
    Entities_name = []
    Entities_salience = []
    Entities_score = []
    Entities_magnitude = []
    
    response = client.analyze_entity_sentiment(document, encoding_type=encoding_type)
    # Loop through entitites returned from the API
    for entity in response.entities:
        log.info(u"Representative name for the entity: {}".format(entity.name))
        Entities_name.append(entity.name)
        # Get entity type, e.g. PERSON, LOCATION, ADDRESS, NUMBER, et al
        log.info(u"Entity type: {}".format(enums.Entity.Type(entity.type).name))
        # Get the salience score associated with the entity in the [0, 1.0] range
        log.info(u"Salience score: {}".format(entity.salience))
        Entities_salience.append(entity.salience)
        # Get the aggregate sentiment expressed for this entity in the provided document.
        sentiment = entity.sentiment
        log.info(u"Entity sentiment score: {}".format(sentiment.score))
        Entities_score.append(sentiment.score)
        log.info(u"Entity sentiment magnitude: {}".format(sentiment.magnitude))
        Entities_magnitude.append(sentiment.magnitude)
        # Loop over the metadata associated with entity. For many known entities,
        # the metadata is a Wikipedia URL (wikipedia_url) and Knowledge Graph MID (mid).
        # Some entity types may have additional metadata, e.g. ADDRESS entities
        # may have metadata for the address street_name, postal_code, et al.
        for metadata_name, metadata_value in entity.metadata.items():
            log.info(u"{} = {}".format(metadata_name, metadata_value))

        # Loop over the mentions of this entity in the input document.
        # The API currently supports proper noun mentions.
        for mention in entity.mentions:
            log.info(u"Mention text: {}".format(mention.text.content))
            # Get the mention type, e.g. PROPER for proper noun
            log.info(
                u"Mention type: {}".format(enums.EntityMention.Type(mention.type).name)
            )

    # Get the language of the text, which will be the same as
    # the language specified in the request or, if not specified,
    # the automatically-detected language.
    log.info(u"Language of the text: {}".format(response.language))
    return pd.Series([Entities_name,Entities_salience,Entities_score,Entities_magnitude])




# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
# The text to analyze
    good = """Pleasantly surprised, ",via mobile ,30-Dec-19,"Pleasantly surprised, we were worried after 
    reading the reviews. We were a large party, served quickly by very friendly staff and the food was good.
    No one had a bad meal."""

    
    bad = """Meat was old or was not cooked hygenically. Waiter's recommendations are greedy. 
    I think the cook was not from Spain.Food was not eatable"""

    sample_analyze_entity_sentiment(bad)
    


