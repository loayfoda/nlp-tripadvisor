# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 11:29:19 2020

@author: LF05594
"""
import config
import os
from google.cloud import storage
import logging
log = logging.getLogger(__name__)





def test_google_authentication():
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config.google_credentials
    log.info(os.environ['GOOGLE_APPLICATION_CREDENTIALS'])

    # If you don't specify credentials when constructing the client, the
    # client library will look for credentials in the environment.
    storage_client = storage.Client()
    
    # Make an authenticated API request
    buckets = list(storage_client.list_buckets())
    log.info(buckets)