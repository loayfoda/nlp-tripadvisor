# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 11:29:19 2020

@author: LF05594
"""
import config
import os
# Imports the Google Cloud client library
from google.cloud import language
from google.cloud.language import enums
from google.cloud.language import types
import pandas as pd 
import logging
log = logging.getLogger(__name__)

def GoogleSentimentAnalysis(text):
    #Authenticate with google cloud
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = config.google_credentials
    
    
    # Instantiates a client
    client = language.LanguageServiceClient()
    Google_Sentiment = str()
        
    document = types.Document(
        content=text,
        type=enums.Document.Type.PLAIN_TEXT)
    
    # Detects the sentiment of the text
    sentiment = client.analyze_sentiment(document=document).document_sentiment
    
    log.info('Text: {}'.format(text))
    log.info('Sentiment: {}, {}'.format(sentiment.score, sentiment.magnitude))


        
    # decide sentiment as positive, negative or neutral         
    if sentiment.score > 0:
       log.info("positive feelng")
       Google_Sentiment = "Positive"
    elif sentiment.score < 0:
       log.info("negative feelng") 
       Google_Sentiment = "negative"
    else:
       if sentiment.magnitude >= 3:  
           log.info("mixed feelng") 
           Google_Sentiment = "mixed"
       else:
           log.info("neutral") 
           Google_Sentiment = "neutral"  
           
    return pd.Series([sentiment.score,sentiment.magnitude,Google_Sentiment]) 

# states to run the below mentioned function in case the class was explicitly run
if __name__ == '__main__':
# The text to analyze
    good = """Pleasantly surprised, ",via mobile ,30-Dec-19,"Pleasantly surprised, we were worried after 
    reading the reviews. We were a large party, served quickly by very friendly staff and the food was good.
    No one had a bad meal."""

    
    bad = """Meat was old or was not cooked hygenically. Waiter's recommendations are greedy. 
    I think the cook was not from Spain.Food was not eatable"""
    s =  GoogleSentimentAnalysis(good)
    log.info(s)