# -*- coding: utf-8 -*-
"""
Created on Mon Jan 20 10:38:57 2020

@author: LF05594
"""
from selenium import webdriver  
import time
import requests
from bs4 import BeautifulSoup
import csv 
import logging
log = logging.getLogger(__name__)


def get_reviews(URL):
    page = requests.get(URL)

    soup = BeautifulSoup(page.content, 'html.parser')
    base_URL= 'https://www.tripadvisor.com'
    
    review_number = 0
    
    with open('output.csv', 'w', newline='',encoding='utf-8') as csvfile:
         propertyWriter = csv.writer(csvfile, quoting=csv.QUOTE_ALL)
         propertyWriter.writerow(['Review_Title','Review_Method','Review_Date','Review_Text'])
         
         pages = soup.find('div',class_="pageNumbers")
         log.info("total number of pages: {0}".format(len(pages)))

         
         for i in range(0,len(pages)):
             
             new_URL = base_URL + pages.contents[i].get('href') 
             log.info("Current page URL is : {}".format(new_URL))
             log.info("Starting scraping page number :  {}".format(i+1))
             
             
             #Get the content of the current page         
             page = requests.get(new_URL)
             soup = BeautifulSoup(page.content, 'html.parser')
             
             # Beautiful Soup allows you to find that specific element easily by its ID
             results = soup.find(id='taplc_location_reviews_list_resp_rr_resp_0')
             
             reviews = results.find_all('div', class_="review-container")
             log.info("total number of reviews per page: {0}".format(len(reviews))) 
             log.info('\n')
             for rev in reviews:
                    # Each rev is a new BeautifulSoup object.
                    # You can use the same methods on it as you did before.
                    
                    
                    try:
                        #Review Title
                        title_elem = rev.find('div', class_="quote")
                        title_sub = title_elem.find('a',class_="title")
                        title_text = title_sub.find('span',class_="noQuotes").text                
                    except Exception:
                        log.exception("Fatal error in reading review tittle", exc_info=True)
                        title_text = 'NA'
                        
                    try:
                        #Review Date
                        review_date = rev.find('span',class_="ratingDate").get('title')
                    except Exception:
                        log.exception("Fatal error in reading review date", exc_info=True) 
                        review_date = 'NA'
                    
                    try:
                        # Review Method
                        review_method = rev.find('span',class_="viaMobile")
                        if review_method:
                         review_method_text = review_method.text 
                         log.info(review_method_text)
                        else:
                         review_method_text = 'via website'
                         log.info (review_method_text)
                    except Exception:
                        log.exception("Fatal error in reading review method", exc_info=True)
                        review_method_text = 'NA'
                        
                    try:
                        #Get the review text, as well as, checking if pressing more is needed
                        review_more = rev.find('span', class_="taLnk ulBlueLinks")
                        if review_more:
                          log.info(review_more.text)
                          rev_id = rev.find('div', class_="reviewSelector").get('id')
                          log.info(rev_id)
                          driver = webdriver.Chrome("C:/chromedriver/chromedriver.exe")
                          driver.get(new_URL)
                          specificreview = driver.find_element_by_id(rev_id) 
                          next_btn = specificreview.find_element_by_class_name("taLnk.ulBlueLinks")
                          next_btn.click()
                          time.sleep(3)     
                          new_review = driver.find_element_by_id(rev_id)
                          #full = WebDriverWait(driver, 10).until(EC.text_to_be_present_in_element(new_review.find_element_by_xpath("//p[@class='partial_entry']")))    
                          review_text = new_review.find_element_by_class_name("partial_entry").text
                          log.info(review_text)
                          driver.quit()
                        else:
                          log.info ('No more')
                          #Review Text
                          review_text = rev.find('p',class_="partial_entry").text
                          log.info(review_text)
                    except Exception:
                        log.exception("Fatal error in reading review text", exc_info=True) 
                        review_text = 'NA'                
    
                    
                    try:    
                        #Visit Date
                        visit_date = rev.find('div',class_="prw_rup prw_reviews_stay_date_hsx")
                        if visit_date:
                            visit_date_text = visit_date.text                          
                        else:
                            visit_date_text = 'NA'
                    except Exception:
                           log.exception("Fatal error in reading visit date", exc_info=True)
                           visit_date_text = 'NA'
    
                    log.info(title_text)
                    log.info(review_date)
                    log.info("visit date: {0}".format(visit_date_text)) 
                    log.info('\n')
                    propertyWriter.writerow([title_text,review_method_text,review_date,review_text])
                    review_number += 1
     
             print("total number of reviews: {0}".format(review_number)) 
    
