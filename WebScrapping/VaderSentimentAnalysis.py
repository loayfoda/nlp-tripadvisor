# -*- coding: utf-8 -*-
"""
Created on Tue Jan 21 12:36:06 2020

@author: LF05594
"""

# import SentimentIntensityAnalyzer class 
# from vaderSentiment.vaderSentiment module. 

from nltk.sentiment.vader import SentimentIntensityAnalyzer
from nltk.corpus import stopwords 
from nltk.tokenize.treebank import TreebankWordDetokenizer
import pandas as pd
# function to print sentiments 
# of the sentence. 
def sentiment_scores(sentence): 
  
    # Create a SentimentIntensityAnalyzer object. 
    sid_obj = SentimentIntensityAnalyzer() 
    sentiment=str()
    sentiment_value= str()
    # polarity_scores method of SentimentIntensityAnalyzer 
    # oject gives a sentiment dictionary. 
    # which contains pos, neg, neu, and compound scores. 
    sentiment_dict = sid_obj.polarity_scores(sentence) 
      
    print("Overall sentiment dictionary is : ", sentiment_dict) 
    print("sentence was rated as ", sentiment_dict['neg']*100, "% Negative") 
    print("sentence was rated as ", sentiment_dict['neu']*100, "% Neutral") 
    print("sentence was rated as ", sentiment_dict['pos']*100, "% Positive") 
    print("sentence was rated as ", sentiment_dict['compound']*100, "% overall") 
    sentiment_value = sentiment_dict['compound'] *100
    print("Sentence Overall Rated As", end = " ") 
  
    # decide sentiment as positive, negative and neutral 
    if sentiment_dict['compound'] >= 0.05 : 
        print("Positive") 
        sentiment = "Positive"
    elif sentiment_dict['compound'] <= - 0.05 : 
        print("Negative") 
        sentiment = "Negative" 
    else : 
        print("Neutral") 
        sentiment = "Neutral"
        

    return pd.Series([sentiment_value,sentiment])
  
  
    
# Driver code 
if __name__ == "__main__" : 
  
    print("\n1st statement :") 
    good = u"""Pleasantly surprised, we were worried after reading the reviews. 
    We were a large party, served quickly by very friendly staff and the food was good. 
    No one had a bad meal."""
    
    bad = """Meat was old or was not cooked hygenically. Waiter's recommendations are greedy. 
    I think the cook was not from Spain.Food was not eatable"""

    # function calling 
    s = sentiment_scores(bad)
    print(s)
  






